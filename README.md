# Comptes publics de l'indemnité représentatitive de frais de mandat de la député Paula Forteza

Voir la [présentation du projet sur le wiki](https://framagit.org/paula.forteza/irfm.forteza.fr/wikis/home)

## Import des relevés du Crédit Coopératif

Pour récupérer automatiquement les relevés bancaires du compte IRFM, nous utilisons le logiciel libre [Weboob](http://weboob.org/) et plus particulièrement son module [Boobank](http://weboob.org/applications/boobank).

```bash
boobank history 41013088217@creditcooperatif 2017-09-01 -f csv -n 500 > ~/projects/paula/irfm.forteza.fr/creditcooperatif.csv
```

Important : **Modifier la date ci-dessus, par le premier jour du mois à importer.**

Ouvrir le fichier `creditcooperatif.csv` avec un éditeur et
* Remplacer partout "," par "."
* Remplacer partout ";" par ",".

Générer un extrait du journal à partir du fichier `creditcooperatif.csv` :
```bash
hledger -f creditcooperatif.csv print
```

Coller l'extrait ainsi généré à la fin du journal `IRFM_Paula_Forteza.journal` en supprimant au préalable les éventuelles écritures redondantes au début du fichier.

## Import des relevés de la Banque Postale

```bash
boobank history 6894157E020@bp 2017-09-01 -f csv -n 500 > ~/projects/paula/irfm.forteza.fr/banque_postale.csv
```

Important : **Modifier la date ci-dessus, par le premier jour du mois à importer.**

Ouvrir le fichier `banque_postale.csv` avec un éditeur et
* Remplacer partout "," par "."
* Remplacer partout ";" par ",".

Générer un extrait du journal à partir du fichier `banque_postale.csv` :
```bash
hledger -f banque_postale.csv print > banque_postale.journal
```

Coller l'extrait ainsi généré à la fin du journal `IRFM_Paula_Forteza.journal` en supprimant au préalable les éventuelles écritures redondantes au début du fichier.
